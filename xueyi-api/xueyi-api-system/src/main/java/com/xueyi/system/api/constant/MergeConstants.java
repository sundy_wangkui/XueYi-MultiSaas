package com.xueyi.system.api.constant;

/**
 * 主子关联通用常量
 *
 * @author xueyi
 */
public class MergeConstants {

    public static final String MODULE_MENU_GROUP = "module_menu_group";

    public static final String DICT_DATA_GROUP = "dict_data_group";

    public static final String DEPT_POST_GROUP = "dept_post_group";
    public static final String GEN_TABLE_GROUP = "gen_table_group";

}
